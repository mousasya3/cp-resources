/*
 ____ _____ _______ 
|  _ \_   _|__   __|
| |_) || |    | |   
|  _ < | |    | |   
| |_) || |_   | |   
|____/_____|  |_|   

*/

struct Fenwick {
	int bit[N];
	Fenwick() {
		memset(bit,0,sizeof bit);
	}
	void add(int id ,int x){ // ATTENTION: id from 1 not 0
		for(int i = id;i < N;i += i & -i)
			bit[i] += x;
	}
	int getsum(int id){
		int sum = 0;
		for(int i = id;i;i -= i & -i)
			sum += bit[i];
		return sum;
	}
};
