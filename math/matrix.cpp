/*
 __  __       _        _      
|  \/  |     | |      (_)     
| \  / | __ _| |_ _ __ ___  __
| |\/| |/ _` | __| '__| \ \/ /
| |  | | (_| | |_| |  | |>  < 
|_|  |_|\__,_|\__|_|  |_/_/\_\ 

*/

struct mat
{
	int sz;
	vector < vector <int> > v;
	
	mat(int k,bool l = 1)
	{
		v.resize(k);
		for(int i=0;i<k;i++)
		{
			v[i].resize(k);
			v[i][i] = l;
		}
		sz = k;
	}
	
	mat operator*(mat b)
	{
		assert(sz == b.sz);
		mat prod(sz,0);
		for(int i = 0 ; i < sz ; i++)
		{
			for (int j = 0 ; j < sz ; j++)
			{
				for(int k = 0 ; k < sz ; k++)
				{
					prod.v[i][j] = add(prod.v[i][j],mult(v[i][k],b.v[k][j]));
				}
			}
		}
		return prod;
	}
	
	mat pow(ll n)
	{
		if (n == 0) return mat(sz);
		
		mat prod = this->pow(n>>1);
		prod = prod*prod;
		if (n&1) prod = prod*(*this);
		return prod;
	}
};
