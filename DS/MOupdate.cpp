/*
 __  __  ____  _    _           _       _       
|  \/  |/ __ \| |  | |         | |     | |      
| \  / | |  | | |  | |_ __   __| | __ _| |_ ___ 
| |\/| | |  | | |  | | '_ \ / _` |/ _` | __/ _ \
| |  | | |__| | |__| | |_) | (_| | (_| | ||  __/
|_|  |_|\____/ \____/| .__/ \__,_|\__,_|\__\___|
                     | |                        
                     |_|                         
                     
*/

const int BLOCK = 2000;

struct Query {
	int l,r,idx,t;
	bool operator < (Query a) const {
		if (l/BLOCK == a.l/BLOCK)
				return (r/BLOCK == a.r/BLOCK) ? (t < a.t) : (r/BLOCK < a.r/BLOCK);
		return l/BLOCK < a.l/BLOCK
	}
};
