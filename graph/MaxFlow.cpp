/*
 __  __            ______ _               
|  \/  |          |  ____| |              
| \  / | __ ___  _| |__  | | _____      __
| |\/| |/ _` \ \/ /  __| | |/ _ \ \ /\ / /
| |  | | (_| |>  <| |    | | (_) \ V  V / 
|_|  |_|\__,_/_/\_\_|    |_|\___/ \_/\_/  
                                          
*/
#ifndef MAX_FLOW_HPP_INCLUDED
#define MAX_FLOW_HPP_INCLUDED

#include <queue>
#include <vector>
#include <algorithm>

template <class Cap>
struct Graph
{
    int n;

    struct edge
    {
        int v, rv;
        Cap cap;
    };

    struct _edge
    {
        int v, u;
        Cap cap, flow;
    };

    std ::vector<std ::vector<edge>> gr;
    std ::vector<std ::pair<int, int>> pos;
    Graph() : n(0) {}
    Graph(int n) : n(n), gr(n) {}

    int Add_Edge(int v, int u, Cap cap)
    {
        assert(0 <= v && v <= n);
        assert(0 <= u && u <= n);
        assert(0 <= cap);
        int v_id = gr[v].size(), u_id = gr[u].size(), m = pos.size();
        pos.push_back({v, v_id});
        if (v == u)
            v_id++;
        gr[v].push_back({u, u_id, cap});
        gr[u].push_back({v, v_id, 0});
        return m;
    }

    _edge Get_Edge(int i)
    {
        int m = pos.size();
        assert(0 <= i && i < m);
        auto e = gr[pos[i].first][pos[i].second];
        auto re = gr[e.v][e.rv];
        return _edge{pos[i].first, e.v, e.cap + re.cap, re.cap};
    }

    std ::vector<_edge> Edges()
    {
        int m = pos.size();
        std ::vector<_edge> result;
        for (int i = 0; i < m; i++)
            result.push_back(Get_Edge(i));
        return result;
    }

    void Change_Edge(int i, Cap cap, Cap flow)
    {
        int m = pos.size();
        assert(0 <= i && i < m);
        assert(0 <= flow && flow <= cap);
        auto &e = gr[pos[i].first][pos[i].second];
        auto &re = gr[e.v][e.rv];

        e.cap = cap;
        re.cap = flow;
    }

    long long Max_Flow(int start, int end)
    {
        std ::vector<int> level(n);
        auto dfs = [&](auto self, int x, Cap flow_cap)
        {
            if (x == start)
                return flow_cap;
            Cap result_flow = 0;
            for (int i = 0; i < gr[x].size(); i++)
            {
                auto edge = gr[x][i];
                int u = edge.v;
                Cap w = gr[u][edge.rv].cap;
                if (level[x] <= level[u] || w == 0)
                    continue;

                Cap current_flow = self(self, u, std ::min(flow_cap - result_flow, w));

                if (current_flow <= 0)
                    continue;
                gr[x][i].cap += current_flow;
                gr[u][edge.rv].cap -= current_flow;
                result_flow += current_flow;
            }
            return result_flow;
        };
        Cap flow = 0;
        while (true)
        {
            fill(level.begin(), level.end(), -1);
            std ::queue<int> q;

            level[start] = 0;
            q.push(start);
            while (q.size())
            {
                int x = q.front();
                q.pop();
                for (int i = 0; i < gr[x].size(); i++)
                {
                    int u = gr[x][i].v;
                    Cap w = gr[x][i].cap;
                    if (level[u] >= 0 || w == 0)
                        continue;

                    level[u] = level[x] + 1;
                    q.push(u);
                }
            }
            if (level[end] == -1)
                return flow;
            while (true)
            {
                Cap _flow = dfs(dfs, end, std ::numeric_limits<Cap>::max());
                if (_flow == 0)
                    break;
                flow += _flow;
            }
        }
    }
};

#endif // MAX_FLOW_HPP_INCLUDED
