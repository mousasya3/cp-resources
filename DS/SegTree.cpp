/*
  _____         _______            
 / ____|       |__   __|           
| (___   ___  __ _| |_ __ ___  ___ 
 \___ \ / _ \/ _` | | '__/ _ \/ _ \
 ____) |  __/ (_| | | | |  __/  __/
|_____/ \___|\__, |_|_|  \___|\___|
              __/ |                
             |___/                  
             
*/

struct node {
	ll sum;
	node(ll _sum = 0) : sum(_sum) {}
};

struct ST {
	#define lc (id<<1)
	#define rc ((id<<1)|1)
	#define mid (l+(r-l)/2)

	vector<node> seg;
	ST() {seg.resize(n*4);}

	inline node merge(node left,node right) {
		node ret;
		ret.sum = left.sum + right.sum;
		return ret;
	}

	inline void pull(int id) {
		seg[id] = merge(seg[lc],seg[rc]);
	}

	void build(int l = 0,int r = n-1,int id = 1) {
		if (l == r) {
			seg[id].sum = a[l];
			return;
		}
		build(l,mid,lc);
		build(mid+1,r,rc);
		pull(id);
	}

	void upd(int i,int x,int l = 0,int r = n-1,int id = 1) {
		if (l == r) {
			seg[id] = node(x);
			return;
		}
		if (i <= mid) upd(i,x,l,mid,lc);
		else upd(i,x,mid+1,r,rc);
		pull(id);
	}

	node query(int L,int R,int l = 0,int r = n-1,int id = 1) {
		if (l > R || r < L) return node();
		if (l >= L && r <= R) return seg[id];
		return merge(query(L,R,l,mid,lc),query(L,R,mid+1,r,rc));
	}
};
