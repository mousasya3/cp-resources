/*
 _____   _____ _    _   _____       _ _ _                _    
|  __ \ / ____| |  | | |  __ \     | | | |              | |   
| |  | | (___ | |  | | | |__) |___ | | | |__   __ _  ___| | __
| |  | |\___ \| |  | | |  _  // _ \| | | '_ \ / _` |/ __| |/ /
| |__| |____) | |__| | | | \ \ (_) | | | |_) | (_| | (__|   < 
|_____/|_____/ \____/  |_|  \_\___/|_|_|_.__/ \__,_|\___|_|\_\

*/

struct DSU_Rollback {
	vector<int> par, rnk, sz;
	int c;
	struct change {
		int u,v;
		bool merged;
		bool ranked;
		bool checkpoint;
		change(int _u,int _v,bool _merged,bool _ranked) : u(_u),v(_v),
		merged(_merged),ranked(_ranked),\
		checkpoint(0) {}

		change() : checkpoint(1) {}
	};
	bool rollback(change ch) {
		if (ch.checkpoint) return 0;
		if (!ch.merged) return 1;
		if (ch.ranked) rnk[ch.v]--;
		sz[par[ch.u]] -= sz[ch.u];
		par[ch.u] = ch.u;
		c++;
		return 1;
	}
	stack<change> changes;
	void checkpoint() {changes.push(change());}

	DSU(int n) : c(n) {
		rnk.resize(n+1);
		par.resize(n+1);
		sz.resize(n+1);
		for (int i = 1; i <= n; ++i) par[i] = i;
	}
	int find(int i) {
		return (par[i] == i ? i : find(par[i]));
	}
	bool same(int i, int j) {
		return find(i) == find(j);
	}
	int get_size(int i) {
		return sz[find(i)];
	}
	int count() {
		return c;    //connected components
	}
	int merge(int i, int j) {
		if ((i = find(i)) == (j = find(j))) return -1;
		else --c;
		if (rnk[i] > rnk[j]) swap(i, j);
		par[i] = j;
		sz[j] += sz[i];
		bool ranked = 0;
		if (rnk[i] == rnk[j]) {rnk[j]++;ranked=1;}
		changes.push(change(i,j,1,ranked));
		return j;
	}

	void rolltocheckpoint() {
		while(!changes.empty()) {
			auto cur = changes.top();
			changes.pop();
			if (!rollback(cur)) break;
		}
	}
};
