/*
 _      _____          
| |    / ____|   /\    
| |   | |       /  \   
| |   | |      / /\ \  
| |___| |____ / ____ \ 
|______\_____/_/    \_\ 

*/

vector<int> euler;
vector<int> val;
struct SparseTable {
	int st[N][LOG];

	void build() {
		for(int j = 0;j < LOG;j++)
			for(int i = 0;i < 2*n-1;i++) if(i + (1 << j) - 1 < 2*n-1)
				st[i][j] = (j ? min(st[i][j-1], st[i + (1 << (j-1))][j-1]): val[i]);
	}

	int query(int l,int r) {
		if (r < l) swap(l,r);
		int x = 31 - __builtin_clz(r-l+1);
		return min(st[l][x],st[r-(1<<x)+1][x]);
	}
} SP;

vector<int> g[N];

int st[N],rev[N];
void dfs(int u = 1,int p = -1) {
	euler.pb(u);
	for(auto v:g[u]) {
		if (v == p) continue;
		dfs(v);
		euler.pb(u);
	}
}

int getLCA(int u,int v) {
	return rev[SP.query(st[u],st[v])];
}

int main() {
	memset(st,-1,sizeof st);
	dfs();
	for(int i = 0 ; i < sz(euler) ; i++) {
		int u = euler[i];
		if (st[u] == -1) {
			st[u] = i;
			rev[i] = u;
		}
		val.pb(st[u]);
	}
	
	SP.build();
	
	return 0;
}
