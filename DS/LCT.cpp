/*
 _      _____ _______ 
| |    / ____|__   __|
| |   | |       | |   
| |   | |       | |   
| |___| |____   | |   
|______\_____|  |_|   

*/

struct node {
	node *par;
	node *c[2];
	bool rev;
	ll val;
	ll size;
	ll sum;
	ll virt_sum;
	ll virt_size;
	ll added;
	ll cancel;
	node(ll _val) {size=1;sum=val=_val;added=cancel=virt_sum=virt_size=0;par=c[0]=c[1]=0;rev=0;}
	
	bool is_root() {
		return par == 0 || (par->c[0] != this && par->c[1] != this);
	}
	
	void add(ll x) {
		val += x;
		added += x;
		sum += x*size;
		virt_sum += x*virt_size;
	}
	
	void push() {
		if (par) {
			add(par->added - cancel);
			cancel = par->added;
		}
		if (rev) {
			swap(c[0],c[1]);
			for(int i = 0 ; i < 2 ; i++) if (c[i]) c[i]->rev ^= 1;
			rev = 0;
		}
	}
	
	void recalc() {
		size = 1 + virt_size;
		for(int i = 0 ; i < 2 ; i++) if (c[i]) {
			c[i]->push();
			size += c[i]->size;
		}
		sum = val + virt_sum;
		for(int i = 0 ; i < 2 ; i++) if (c[i]) sum += c[i]->sum;
	}
	
	int side() {
		return (par->c[1] == this);
	}

	void rotate() {
		
		node* p = par;
		node* g = p->par;

		p->push();
		push();
		int i = side();
		if (c[i^1]) c[i^1]->push();
		
		if (!p->is_root()) {
			g->c[p->side()] = this;
		}
		
		
		p->c[i] = c[i^1];
		c[i^1] = p;
		if (p->c[i]) {
			p->c[i]->par = p;
			p->c[i]->cancel = p->added;
		}
		
		p->par = this;
		p->cancel = added;
		par = g;
		cancel = (g?g->added:0);
		p->recalc();
		recalc();
		if (g) g->recalc();
	}
	
	void splay() {
		while(!is_root()) {
			node* p = par;
			if (!p->is_root()) {
				(side() == p->side() ? p : this)->rotate();
			}
			rotate();
		}
		push();
	}
};

node* access(node* cur) {
	node* last = 0;
	for(node* p = cur;p;p = p->par) {
		p->splay();
		if (last) last->push();
		if (p->c[1]) p->c[1]->push();
		p->virt_size += -(last?last->size:0) + (p->c[1]?p->c[1]->size:0);
		p->virt_sum += -(last?last->sum:0) + (p->c[1]?p->c[1]->sum:0);
		p->c[1] = last;
		p->recalc();
		last = p;
	}
	cur->splay();
	return last;
}

void make_root(node *cur) {
	access(cur);
	if (cur->c[0]) {
		cur->virt_size += cur->c[0]->size;
		cur->virt_sum += cur->c[0]->sum;
		cur->c[0]->rev ^= 1;
		cur->c[0]->push();
		cur->c[0] = 0;
	}
	cur->recalc();
}

void cut(node *u,node *v) {
	make_root(u);
	access(v);
	if (v->c[0]) {
		v->c[0]->push();
		v->c[0]->par = 0;
		v->c[0]->cancel = 0;
		v->c[0] = 0;
	}
	v->recalc();
}

void link(node *u,node *v) {
	make_root(v);
	access(u);
	v->par = u;
	v->cancel = u->added;
	u->virt_size += v->size;
	u->virt_sum += v->sum;
	u->recalc();
}

bool connected(node* u,node* v) {
	access(u);
	access(v);
	return u->par;
}
