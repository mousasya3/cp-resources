/*
  _____         _______            _                     
 / ____|       |__   __|          | |                    
| (___   ___  __ _| |_ __ ___  ___| |     __ _ _____   _ 
 \___ \ / _ \/ _` | | '__/ _ \/ _ \ |    / _` |_  / | | |
 ____) |  __/ (_| | | | |  __/  __/ |___| (_| |/ /| |_| |
|_____/ \___|\__, |_|_|  \___|\___|______\__,_/___|\__, |
              __/ |                                 __/ |
             |___/                                 |___/ 
             
 */

struct node {
	ll sum,lazy;
	node(ll _sum = 0) : sum(_sum), lazy(0) {}
};

struct ST {
	#define lc (id<<1)
	#define rc ((id<<1)|1)
	#define mid (l+(r-l)/2)

	vector<node> seg;
	ST() {seg.resize(n*4);}

	inline void push(int l,int r,int id) {
		if (seg[id].lazy == 0) return;
		if (l != r) {
			seg[lc].lazy += seg[id].lazy;
			seg[rc].lazy += seg[id].lazy;
		}
		seg[id].sum += seg[id].lazy*(r-l+1);
		seg[id].lazy = 0;
	}

	inline node merge(node left,node right) {
		node ret;
		ret.sum = left.sum + right.sum;
		return ret;
	}

	inline void pull(int id) {
		seg[id] = merge(seg[lc],seg[rc]);
	}

	void build(int l = 0,int r = n-1,int id = 1) {
		if (l == r) {
			seg[id].sum = a[l];
			return;
		}
		build(l,mid,lc);
		build(mid+1,r,rc);
		pull(id);
	}

	void upd(int L,int R,int x,int l = 0,int r = n-1,int id = 1) {
		push(l,r,id);
		if (l > R || r < L) return;
		if (l >= L && r <= R) {
			seg[id].lazy += x;
			push(l,r,id);
			return;
		}
		upd(L,R,x,l,mid,lc);
		upd(L,R,x,mid+1,r,rc);
		pull(id);
	}

	node query(int L,int R,int l = 0,int r = n-1,int id = 1) {
		push(l,r,id);
		if (l > R || r < L) return node();
		if (l >= L && r <= R) return seg[id];
		return merge(query(L,R,l,mid,lc),query(L,R,mid+1,r,rc));
	}
};
