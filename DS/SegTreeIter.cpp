/*
   _____         _______            
 / ____|       |__   __|           
| (___   ___  __ _| |_ __ ___  ___ 
 \___ \ / _ \/ _` | | '__/ _ \/ _ \
 ____) |  __/ (_| | | | |  __/  __/
|_____/ \___|\__, |_|_|  \___|\___|
              __/ |                
             |___/                 
             
*/

struct ST {
	int t[2 * N];
	ST() {
		memset(t, 0, sizeof t);
	}
	inline int combine(int l, int r) {
		return l + r;
	}
	void build() {
		for(int i = 0; i < n; i++) t[i + n] = a[i];
		for(int i = n - 1; i > 0; --i) t[i] = combine(t[i << 1], t[i << 1 | 1]);
	}
	void upd(int p, int v) {
		for (t[p += n] = v; p >>= 1; ) t[p] = combine(t[p << 1], t[p << 1 | 1]);
	}
	int query(int l, int r) {
		r++;
		int resl = 0, resr = 0;
		for(l += n, r += n; l < r; l >>= 1, r >>= 1) {
			if(l & 1) resl = combine(resl, t[l++]);
			if(r & 1) resr = combine(t[--r], resr);
		}
		return combine(resl, resr);
	}
};
