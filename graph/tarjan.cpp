/*
 _______         _             
|__   __|       (_)            
   | | __ _ _ __ _  __ _ _ __  
   | |/ _` | '__| |/ _` | '_ \ 
   | | (_| | |  | | (_| | | | |
   |_|\__,_|_|  | |\__,_|_| |_|
               _/ |            
              |__/             
               
*/


pair<int,int> edges[N];
vector<pair<int,int>> g[N]; // {v, id}
int cnt = 0;
int num[N],low[N];
bool bridge[N],cutpoint[N];

void dfs(int u,int p = -1) {
    num[u] = low[u] = ++cnt;
    for(auto vv:g[u]) {
        int v = vv.F;
        int id = vv.S;
        if (id == p) continue;
        if (!num[v]) {
            dfs(v,id);
            if (low[v] > num[u]) {
                br[id] = 1;
            }
            if (low[v] >= num[v]) {
                cutpoint[v] = 1;
            }
            low[u] = min(low[u],low[v]);
        }
        else low[u] = min(low[u],num[v]);
    }
}
