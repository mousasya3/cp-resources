/*
 _  __            _   _     
| |/ /           | | | |    
| ' / _ __  _   _| |_| |__  
|  < | '_ \| | | | __| '_ \ 
| . \| | | | |_| | |_| | | |
|_|\_\_| |_|\__,_|\__|_| |_| 

*/

// dp[i][j] = mink(dp[i][k]+dp[k+1][j]+C(i,j))
// opt(i,j) = optimal k

// condition:
// opt(i,j-1) <= opt(i,j) <= opt(i+1,j)
// or:
// a <= b <= c <= d
// C(b,c) <= C(a,d)
// C(a,c) + C(b,d) <= C(a,d) + C(b,c)

// O(n^2)

int solve() {
    int N;
    ... // read N and input
    int dp[N][N], opt[N][N];

    auto C = [&](int i, int j) {
        ... // Implement cost function C.
    };

    for (int i = 0; i < N; i++) {
        opt[i][i] = i;
        ... // Initialize dp[i][i] according to the problem
    }

    for (int i = N-2; i >= 0; i--) {
        for (int j = i+1; j < N; j++) {
            int mn = INT_MAX;
            int cost = C(i, j);
            for (int k = opt[i][j-1]; k <= min(j-1, opt[i+1][j]); k++) {
                if (mn >= dp[i][k] + dp[k+1][j] + cost) {
                    opt[i][j] = k; 
                    mn = dp[i][k] + dp[k+1][j] + cost; 
                }
            }
            dp[i][j] = mn; 
        }
    }

    cout << dp[0][N-1] << endl;
}
