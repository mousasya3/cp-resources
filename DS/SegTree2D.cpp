/*
  _____         _______            ___  _____  
 / ____|       |__   __|          |__ \|  __ \ 
| (___   ___  __ _| |_ __ ___  ___   ) | |  | |
 \___ \ / _ \/ _` | | '__/ _ \/ _ \ / /| |  | |
 ____) |  __/ (_| | | | |  __/  __// /_| |__| |
|_____/ \___|\__, |_|_|  \___|\___|____|_____/ 
              __/ |                            
             |___/                              
             
*/

int seg[8020][8020];
void buildy(int lx,int rx,int idx,int l=1,int r=m,int idy=1)
{
    if(l==r)
    {
        if(lx==rx)
        {
            seg[idx][idy]=b[lx][l];
        }
        else
        {
            seg[idx][idy]=seg[idx*2][idy]+seg[idx*2+1][idy];
        }
    }
    else
    {
        buildy(lx,rx,idx,l,tmp,2*idy);
        buildy(lx,rx,idx,tmp+1,r,2*idy+1);
        seg[idx][idy]=seg[idx][idy*2]+seg[idx][idy*2+1];
    }
}
void buildx(int l=1,int r=n,int idx=1)
{
    if(l!=r)
    {
        buildx(l,tmp,2*idx);
        buildx(tmp+1,r,2*idx+1);
    }
    buildy(l,r,idx);
}
int gety(int idx,int ly,int ry,int l=1,int r=m,int idy=1)
{
    if(ly>r || ry<l)
        return 0;
    if(ly<=l && ry>=r)
        return seg[idx][idy];
    return gety(idx,ly,ry,l,tmp,2*idy)+gety(idx,ly,ry,tmp+1,r,2*idy+1);
}
int getx(int lx,int rx,int ly,int ry,int l=1,int r=n,int idx=1)
{
    if(lx>r || rx<l)
        return 0;
    if(lx<=l && rx>=r)
        return gety(idx,ly,ry);
    return getx(lx,rx,ly,ry,l,tmp,2*idx)+getx(lx,rx,ly,ry,tmp+1,r,2*idx+1);
}
void updatey(int v,int idx,int lx,int rx,int ly,int l=1,int r=m,int idy=1)
{
    if(l==r)
    {
        if(lx==rx)
            seg[idx][idy]=v;
        else
            seg[idx][idy]=seg[idx*2][idy]+seg[idx*2+1][idy];
        return ;
    }
    if(ly<=tmp)
        updatey(v,idx,lx,rx,ly,l,tmp,idy*2);
    else
        updatey(v,idx,lx,rx,ly,tmp+1,r,2*idy+1);
    seg[idx][idy]=seg[idx][idy*2]+seg[idx][idy*2+1];
}
void updatex(int v,int lx,int ly,int l=1,int r=n,int idx=1)
{

    if(l!=r)
    {
        if(lx<=tmp)
            updatex(v,lx,ly,l,tmp,idx*2);
        else
            updatex(v,lx,ly,tmp+1,r,idx*2+1);
    }
    updatey(v,idx,l,r,ly);
    return ;

}
