/*
 __  __       _   _     
|  \/  |     | | | |    
| \  / | __ _| |_| |__  
| |\/| |/ _` | __| '_ \ 
| |  | | (_| | |_| | | |
|_|  |_|\__,_|\__|_| |_|

*/

// ax + by = __gcd(a, b)
// returns __gcd(a, b)
ll extended_euclid(ll a, ll b, ll &x, ll &y) {
	ll xx = y = 0;
	ll yy = x = 1;
	while (b) {
		ll q = a / b;
		ll t = b; b = a % b; a = t;
		t = xx; xx = x - q * xx; x = t;
		t = yy; yy = y - q * yy; y = t;
	}
	return a;
}

// solves a.x + b.y = c
bool diophantine(ll a, ll b, ll c, ll &x0, ll &y0, ll &g) {
    g = extended_euclid(abs(a), abs(b), x0, y0);
    if (c % g) {
        return false;
    }

    x0 *= c / g;
    y0 *= c / g;
    if (a < 0) x0 = -x0;
    if (b < 0) y0 = -y0;
    return true;
}

// finds x such that x % m1 = a1, x % m2 = a2. m1 and m2 may not be coprime
// here, x is unique modulo m = lcm(m1, m2). returns (x, m). on failure, m = -1.
pair<ll, ll> CRT(ll a1, ll m1, ll a2, ll m2) {
	ll p, q;
	ll g = extended_euclid(m1, m2, p, q);
	if (a1 % g != a2 % g) return make_pair(0, -1);
	T m = m1 / g * m2;
	p = (p % m + m) % m;
	q = (q % m + m) % m;
	return make_pair((p * a2 % m * (m1 / g) % m + q * a1 % m * (m2 / g) % m) %  m, m);
}

// find inverse a mod m
ll inverse(ll a, ll m) {
	ll x, y;
	ll g = extended_euclid(a, m, x, y);
	if (g != 1) return -1;
	return (x % m + m) % m;
}

// returns nCr modulo mod where mod is a prime
// Complexity: log(n)
int lucas(ll n, ll r) {
	if (r > n) return 0;
	if (n < mod) return ncr(n, r);
	return mult(lucas(n / mod, r / mod),lucas(n % mod, r % mod));
}

vector<int> primes =
{53,97,193,389,769,1543,3079,6151,12289,24593,49157,98317,196613,393241,
786433,1572869,3145739,6291469,12582917,25165843,50331653,100663319,201326611,
402653189,805306457,1610612741};


vector<int> getdivisors(int x) {
	int cur = x; vector <int> v;
	v.pb(x);
	while(cur){
		int tmp = x + 1;
		if(cur > 1) tmp = (x) / cur + 1;
		if(tmp > k) break;
		cur = x / tmp;
		v.pb(cur);
	}
	v.pb(-1);
	return v;
}
